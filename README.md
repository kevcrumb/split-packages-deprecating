## The Split Linux packages collection

Files required for building and running Split Linux;

### Building

```
for package in srcpkgs/*; do
	./xbps-src pkg `basename "$package"`
done
```
